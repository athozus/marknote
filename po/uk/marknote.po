# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the marknote package.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2023, 2024.
msgid ""
msgstr ""
"Project-Id-Version: marknote\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-08-12 00:39+0000\n"
"PO-Revision-Date: 2024-07-17 09:45+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 23.04.3\n"

#: application.cpp:34
#, kde-format
msgctxt "@action:inmenu"
msgid "New Notebook"
msgstr "Створити записник"

#: application.cpp:43
#, kde-format
msgctxt "@action:inmenu"
msgid "New Note"
msgstr "Створити нотатку"

#: application.cpp:59
#, kde-format
msgctxt "@action:inmenu"
msgid "Import from KNotes"
msgstr "Імпортувати з KNotes"

#: application.cpp:66
#, kde-format
msgctxt "@action:inmenu"
msgid "Import from Maildir"
msgstr "Імпортувати з Maildir"

#: maildirimport.cpp:91
#, kde-format
msgctxt "@status"
msgid "An error occurred while writing to '%1'"
msgstr "Під час спроби записати дані до «%1» сталася помилка"

#: main.cpp:66 qml/Main.qml:345
#, kde-format
msgctxt "Application name"
msgid "Marknote"
msgstr "Marknote"

#: main.cpp:68
#, kde-format
msgid "Note taking application"
msgstr "Програма для роботи з нотатками"

#: main.cpp:70
#, kde-format
msgid "© 2023 Mathis Brüchert"
msgstr "© Mathis Brüchert, 2023"

#: main.cpp:71
#, kde-format
msgid "Mathis Brüchert"
msgstr "Mathis Brüchert"

#: main.cpp:72 main.cpp:77
#, kde-format
msgid "Maintainer"
msgstr "Супровідник"

#: main.cpp:76
#, kde-format
msgid "Carl Schwan"
msgstr "Carl Schwan"

#: main.cpp:81
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Юрій Чорноіван"

#: main.cpp:81
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "yurchor@ukr.net"

#: notesmodel.cpp:112
#, kde-format
msgctxt "@info:status"
msgid "Unable to rename note. A note already exists with the same name."
msgstr "Не вдалося перейменувати нотатку. Нотатка з такою назвою вже існує."

#: qml/EditPage.qml:77 qml/EditPage.qml:90 qml/EditPage.qml:551
#: qml/EditPage.qml:564
#, kde-format
msgid "undo"
msgstr "повернути"

#: qml/EditPage.qml:146
#, kde-format
msgid "Focus Mode"
msgstr "Режим фокусування"

#: qml/EditPage.qml:161 qml/NativeWindowMenu.qml:21 qml/NotesPage.qml:169
#: qml/WelcomePage.qml:29
#, kde-format
msgctxt "@action:menu"
msgid "Exit Full Screen"
msgstr "Вийти з повноекранного режиму"

#: qml/EditPage.qml:212
#, kde-format
msgctxt "@action:button"
msgid "Bold"
msgstr "Жирний"

#: qml/EditPage.qml:231
#, kde-format
msgctxt "@action:button"
msgid "Italic"
msgstr "Курсив"

#: qml/EditPage.qml:250
#, kde-format
msgctxt "@action:button"
msgid "Underline"
msgstr "Підкреслити"

#: qml/EditPage.qml:264
#, kde-format
msgctxt "@action:button"
msgid "Strikethrough"
msgstr "Перекреслення"

#: qml/EditPage.qml:282
#, kde-format
msgctxt "@action:button"
msgid "Increase List Level"
msgstr "Підвищити рівень у списку"

#: qml/EditPage.qml:293
#, kde-format
msgctxt "@action:button"
msgid "Decrease List Level"
msgstr "Знизити рівень у списку"

#: qml/EditPage.qml:335
#, kde-format
msgctxt "@item:inmenu no list style"
msgid "No list"
msgstr "Не є списком"

#: qml/EditPage.qml:336
#, kde-format
msgctxt "@item:inmenu unordered style"
msgid "Unordered list"
msgstr "Невпорядкований список"

#: qml/EditPage.qml:337
#, kde-format
msgctxt "@item:inmenu ordered style"
msgid "Ordered list"
msgstr "Впорядкований список"

#: qml/EditPage.qml:347
#, kde-format
msgctxt "@action:button"
msgid "Insert checkbox"
msgstr "Вставити поле для позначки"

#: qml/EditPage.qml:362
#, kde-format
msgctxt "@action:button"
msgid "Insert link"
msgstr "Вставити посилання"

#: qml/EditPage.qml:378
#, kde-format
msgctxt "@action:button"
msgid "Insert image"
msgstr "Вставити зображення"

#: qml/EditPage.qml:391
#, kde-format
msgctxt "@action:button"
msgid "Insert table"
msgstr "Вставити таблицю"

#: qml/EditPage.qml:411
#, kde-format
msgctxt "@item:inmenu no heading"
msgid "Basic text"
msgstr "Основний текст"

#: qml/EditPage.qml:412
#, kde-format
msgctxt "@item:inmenu heading level 1 (largest)"
msgid "Title"
msgstr "Заголовок"

#: qml/EditPage.qml:413
#, kde-format
msgctxt "@item:inmenu heading level 2"
msgid "Subtitle"
msgstr "Підзаголовок"

#: qml/EditPage.qml:414
#, kde-format
msgctxt "@item:inmenu heading level 3"
msgid "Section"
msgstr "Розділ"

#: qml/EditPage.qml:415
#, kde-format
msgctxt "@item:inmenu heading level 4"
msgid "Subsection"
msgstr "Підрозділ"

#: qml/EditPage.qml:416
#, kde-format
msgctxt "@item:inmenu heading level 5"
msgid "Paragraph"
msgstr "Абзац"

#: qml/EditPage.qml:417
#, kde-format
msgctxt "@item:inmenu heading level 6 (smallest)"
msgid "Subparagraph"
msgstr "Підпараграф"

#: qml/EditPage.qml:597
#, kde-format
msgid "Format"
msgstr "Формат"

#: qml/EditPage.qml:601
#, kde-format
msgid "Lists"
msgstr "Списки"

#: qml/EditPage.qml:605
#, kde-format
msgid "Insert"
msgstr "Вставити"

#: qml/FormIconDelegate.qml:17
#, kde-format
msgctxt "@action:button"
msgid "Icon"
msgstr "Піктограма"

#: qml/GlobalMenuBar.qml:18
#, kde-format
msgctxt "@action:menu"
msgid "Settings"
msgstr "Параметри"

#: qml/ImageDialog.qml:18
#, kde-format
msgctxt "@title:window"
msgid "Insert Image"
msgstr "Вставлення зображення"

#: qml/ImageDialog.qml:24 qml/ImportMaildirDialog.qml:56
#, kde-format
msgctxt "@title:window"
msgid "Select an image"
msgstr "Виберіть зображення"

#: qml/ImageDialog.qml:27
#, kde-format
msgid "Image files (*.jpg *.jpeg *.png *.svg *.webp)"
msgstr "файли зображень (*.jpg *.jpeg *.png *.svg *.webp)"

#: qml/ImageDialog.qml:27
#, kde-format
msgid "All files (*)"
msgstr "усі файли (*)"

#: qml/ImageDialog.qml:42
#, kde-format
msgctxt "@label:textbox"
msgid "Image Location:"
msgstr "Розташування зображення:"

#: qml/ImageDialog.qml:53
#, kde-format
msgctxt "@label:textbox"
msgid "Quick Sketch"
msgstr "Швидкий ескіз"

#: qml/ImportMaildirDialog.qml:32
#, kde-format
msgctxt "@title:window"
msgid "Import from Maildir"
msgstr "Імпортування з Maildir"

#: qml/ImportMaildirDialog.qml:32
#, kde-format
msgctxt "@title:window"
msgid "Import from KNotes"
msgstr "Імпортування з KNotes"

#: qml/ImportMaildirDialog.qml:37 qml/NotebookMetadataDialog.qml:39
#, kde-format
msgctxt "@label:textbox Notebook name"
msgid "Name:"
msgstr "Назва:"

#: qml/ImportMaildirDialog.qml:50
#, kde-format
msgctxt "@label:textbox Notebook name"
msgid "Maildir location:"
msgstr "Розташування Maildir:"

#: qml/ImportMaildirDialog.qml:103
#, kde-format
msgctxt "@action:button"
msgid "Import"
msgstr "Імпортувати"

#: qml/LinkDialog.qml:17
#, kde-format
msgctxt "@title:window"
msgid "Insert Link"
msgstr "Вставити посилання"

#: qml/LinkDialog.qml:23
#, kde-format
msgctxt "@label:textbox"
msgid "Link Text:"
msgstr "Текст посилання:"

#: qml/LinkDialog.qml:31
#, kde-format
msgctxt "@label:textbox"
msgid "Link URL:"
msgstr "Адреса URL посилання:"

#: qml/Main.qml:104
#, kde-format
msgctxt "@info:status"
msgid "Unable to create a new note, you need to create a notebook first."
msgstr "Не вдалося створити нотатку. Спочатку вам слід створити записник."

#: qml/Main.qml:104
#, kde-format
msgctxt "@action:button"
msgid "Create Notebook"
msgstr "Створити записник"

#: qml/Main.qml:271
#, kde-format
msgctxt "@title:menu"
msgid "Import"
msgstr "Імпортувати"

#: qml/Main.qml:285
#, kde-format
msgctxt "@title:menu"
msgid "Sort Notes List"
msgstr "Упорядкування списку нотаток"

#: qml/Main.qml:291
#, kde-format
msgid "by Name"
msgstr "за назвою"

#: qml/Main.qml:305
#, kde-format
msgid "by Date"
msgstr "за датою"

#: qml/Main.qml:330
#, kde-format
msgid "Collapse Sidebar"
msgstr "Згорнути бічну панель"

#: qml/Main.qml:330
#, kde-format
msgid "Expand Sidebar"
msgstr "Розгорнути бічну панель"

#: qml/Main.qml:399
#, kde-format
msgid "Your Notebooks"
msgstr "Додати записники"

#: qml/NativeEditMenu.qml:10
#, kde-format
msgctxt "@action:menu"
msgid "Edit"
msgstr "Зміни"

#: qml/NativeEditMenu.qml:39
#, kde-format
msgctxt "text editing menu action"
msgid "Undo"
msgstr "Скасувати"

#: qml/NativeEditMenu.qml:49
#, kde-format
msgctxt "text editing menu action"
msgid "Redo"
msgstr "Повторити"

#: qml/NativeEditMenu.qml:62
#, kde-format
msgctxt "text editing menu action"
msgid "Cut"
msgstr "Вирізати"

#: qml/NativeEditMenu.qml:72
#, kde-format
msgctxt "text editing menu action"
msgid "Copy"
msgstr "Копіювати"

#: qml/NativeEditMenu.qml:82
#, kde-format
msgctxt "text editing menu action"
msgid "Paste"
msgstr "Вставити"

#: qml/NativeEditMenu.qml:92
#, kde-format
msgctxt "text editing menu action"
msgid "Delete"
msgstr "Вилучити"

#: qml/NativeEditMenu.qml:105
#, kde-format
msgctxt "text editing menu action"
msgid "Select All"
msgstr "Позначити все"

#: qml/NativeFileMenu.qml:11
#, kde-format
msgctxt "@action:menu"
msgid "File"
msgstr "Файл"

#: qml/NativeHelpMenu.qml:11
#, kde-format
msgctxt "@action:menu"
msgid "Help"
msgstr "Довідка"

#: qml/NativeWindowMenu.qml:13
#, kde-format
msgctxt "@action:menu"
msgid "Window"
msgstr "Вікно"

#: qml/NativeWindowMenu.qml:21
#, kde-format
msgctxt "@action:menu"
msgid "Enter Full Screen"
msgstr "Увійти до повноекранного режиму"

#: qml/NotebookContextMenu.qml:16
#, kde-format
msgctxt "@action:inmenu"
msgid "Edit Notebook"
msgstr "Редагувати записник"

#: qml/NotebookContextMenu.qml:33 qml/NotebookDeleteAction.qml:15
#, kde-format
msgctxt "@action:inmenu"
msgid "Delete Notebook"
msgstr "Вилучити записник"

#: qml/NotebookDeleteDialog.qml:31
#, kde-format
msgctxt "@title:window"
msgid "Delete Notebook"
msgstr "Вилучити записник"

#: qml/NotebookDeleteDialog.qml:38
#, kde-format
msgid ""
"Are you sure you want to delete the Notebook <b> %1 </b>? This will delete "
"the content of <b>%2</b> definitively."
msgstr ""
"Ви справді хочете вилучити записник <b>%1</b>? У результаті буде повністю "
"вилучено вміст <b>%2</b>."

#: qml/NotebookMetadataDialog.qml:32
#, kde-format
msgctxt "@title:window"
msgid "New Notebook"
msgstr "Новий записник"

#: qml/NotebookMetadataDialog.qml:32
#, kde-format
msgctxt "@title:window"
msgid "Edit Notebook"
msgstr "Редагування записника"

#: qml/NoteMetadataDialog.qml:27
#, kde-format
msgctxt "@title:window"
msgid "New Note"
msgstr "Нова нотатка"

#: qml/NoteMetadataDialog.qml:27
#, kde-format
msgctxt "@title:window"
msgid "Edit Note"
msgstr "Редагування нотатки"

#: qml/NoteMetadataDialog.qml:58
#, kde-format
msgctxt "@label:textbox Note name"
msgid "Name:"
msgstr "Назва:"

#: qml/NotesPage.qml:48
#, kde-format
msgid "Add note"
msgstr "Додати нотатку"

#: qml/NotesPage.qml:132
#, kde-format
msgid "Exit Search (%1)"
msgstr "Вийти з пошуку (%1)"

#: qml/NotesPage.qml:132
#, kde-format
msgid "Search notes (%1)"
msgstr "Шукати нотатки (%1)"

#: qml/NotesPage.qml:189
#, kde-format
msgctxt "@title:window"
msgid "Delete Note"
msgstr "Вилучення нотатки"

#: qml/NotesPage.qml:202
#, kde-format
msgid ""
"Are you sure you want to delete the note <b> %1 </b>? This will delete the "
"file <b>%2</b> definitively."
msgstr ""
"Ви дійсно бажаєте вилучити нотатку <b>%1</b>? У результаті буде повністю "
"вилучено файл <b>%2</b>."

#: qml/NotesPage.qml:316
#, kde-format
msgctxt "@action:inmenu"
msgid "Rename Note"
msgstr "Перейменувати нотатку"

#: qml/NotesPage.qml:322
#, kde-format
msgctxt "@action:inmenu"
msgid "Delete Note"
msgstr "Вилучити нотатку"

#: qml/NotesPage.qml:333
#, kde-format
msgctxt "@action:inmenu"
msgid "Export to HTML"
msgstr "Експортувати до HTML"

#: qml/NotesPage.qml:339
#, kde-format
msgctxt "@title:window"
msgid "Export to HTML"
msgstr "Експортування до HTML"

#: qml/NotesPage.qml:340
#, kde-format
msgid "HTML file (*.html)"
msgstr "файл HTML (*.html)"

#: qml/NotesPage.qml:346
#, kde-format
msgctxt "@action:inmenu"
msgid "Export to PDF"
msgstr "Експортувати до PDF"

#: qml/NotesPage.qml:352
#, kde-format
msgctxt "@title:window"
msgid "Export to PDF"
msgstr "Експортування до PDF"

#: qml/NotesPage.qml:353
#, kde-format
msgid "PDF file (*.pdf)"
msgstr "файл PDF (*.pdf)"

#: qml/NotesPage.qml:359
#, kde-format
msgctxt "@action:inmenu"
msgid "Export to ODT"
msgstr "Експортувати в ODT"

#: qml/NotesPage.qml:365
#, kde-format
msgctxt "@title:window"
msgid "Export to ODT"
msgstr "Експортувати в ODT"

#: qml/NotesPage.qml:366
#, kde-format
msgid "ODF Text Document (*.odt)"
msgstr "текстовий документ ODF (*.odt)"

#: qml/NotesPage.qml:496
#, kde-format
msgid "Add a note!"
msgstr "Додайте нотатку!"

#: qml/TableDialog.qml:18
#, kde-format
msgctxt "@title:window"
msgid "Insert Table"
msgstr "Вставлення таблиці"

#: qml/TextFieldContextMenu.qml:226
#, kde-format
msgctxt "@action:inmenu"
msgid "Ignore"
msgstr "Ігнорувати"

#: qml/TextFieldContextMenu.qml:241
#, kde-format
msgctxt "@action:inmenu"
msgid "Spell Check"
msgstr "Перевірка правопису"

#: qml/TextFieldContextMenu.qml:258
#, kde-format
msgctxt "@inmenu"
msgid "Open Link"
msgstr "Відкрити посилання"

#: qml/TextFieldContextMenu.qml:268
#, kde-format
msgctxt "@inmenu"
msgid "Insert"
msgstr "Вставити"

#: qml/TextFieldContextMenu.qml:290
#, kde-format
msgctxt "@inmenu"
msgid "Remove"
msgstr "Вилучити"

#: qml/TextFieldContextMenu.qml:310
#, kde-format
msgctxt "@action:inmenu"
msgid "Undo"
msgstr "Вернути"

#: qml/TextFieldContextMenu.qml:325
#, kde-format
msgctxt "@action:inmenu"
msgid "Redo"
msgstr "Повторити"

#: qml/TextFieldContextMenu.qml:343
#, kde-format
msgctxt "@action:inmenu"
msgid "Cut"
msgstr "Вирізати"

#: qml/TextFieldContextMenu.qml:358
#, kde-format
msgctxt "@action:inmenu"
msgid "Copy"
msgstr "Копіювати"

#: qml/TextFieldContextMenu.qml:373
#, kde-format
msgctxt "@action:inmenu"
msgid "Paste"
msgstr "Вставити"

#: qml/TextFieldContextMenu.qml:388
#, kde-format
msgctxt "@action:inmenu"
msgid "Delete"
msgstr "Вилучити"

#: qml/TextFieldContextMenu.qml:407
#, kde-format
msgctxt "@action:inmenu"
msgid "Select All"
msgstr "Позначити все"

#: qml/WelcomePage.qml:22
#, kde-format
msgid "Start by creating your first notebook!"
msgstr "Розпочніть зі створення вашого першого записника!"

#: settings/MarkNoteGeneralPage.qml:16
#, kde-format
msgctxt "@title:window"
msgid "General"
msgstr "Загальне"

#: settings/MarkNoteGeneralPage.qml:19
#, kde-format
msgid "General theme"
msgstr "Загальна тема"

#: settings/MarkNoteGeneralPage.qml:26
#, kde-format
msgid "Color theme"
msgstr "Тема кольорів"

#: settings/MarkNoteGeneralPage.qml:40
#, kde-format
msgid "Editor Settings"
msgstr "Параметри редактора"

#: settings/MarkNoteGeneralPage.qml:45
#, kde-format
msgctxt "@label:textbox"
msgid "Notes Directory:"
msgstr "Каталог нотаток:"

#: settings/MarkNoteGeneralPage.qml:54
#, kde-format
msgctxt "@title:window"
msgid "Select the notes directory"
msgstr "Виберіть каталог нотаток"

#: settings/MarkNoteGeneralPage.qml:69
#, kde-format
msgctxt "@label:spinbox"
msgid "Font family:"
msgstr "Гарнітура шрифту:"

#: settings/MarkNoteGeneralPage.qml:110
#, kde-format
msgctxt "@label:spinbox"
msgid "Font size:"
msgstr "Розмір шрифту:"

#: settings/MarkNoteSettings.qml:15
#, kde-format
msgctxt "@action:button"
msgid "General"
msgstr "Загальне"

#: settings/MarkNoteSettings.qml:21
#, kde-format
msgctxt "@action:button"
msgid "About MarkNote"
msgstr "Про Marknote"

#: settings/MarkNoteSettings.qml:27
#, kde-format
msgctxt "@action:button"
msgid "About KDE"
msgstr "Про KDE"

#: tableactionhelper.cpp:118
#, kde-format
msgid "Row Below"
msgstr "Рядок нижче"

#: tableactionhelper.cpp:124
#, kde-format
msgid "Row Above"
msgstr "Рядок вище"

#: tableactionhelper.cpp:130
#, kde-format
msgid "Column Before"
msgstr "Стовпчик до"

#: tableactionhelper.cpp:137
#, kde-format
msgid "Column After"
msgstr "Стовпчик після"

#: tableactionhelper.cpp:143
#, kde-format
msgid "Row"
msgstr "Рядок"

#: tableactionhelper.cpp:149
#, kde-format
msgid "Column"
msgstr "Стовпчик"

#: tableactionhelper.cpp:155
#, kde-format
msgid "Cell Contents"
msgstr "Вміст комірки"

#~ msgctxt "@label:textbox"
#~ msgid "Number of Rows:"
#~ msgstr "Кількість рядків:"

#~ msgctxt "@label:textbox"
#~ msgid "Number of Columns:"
#~ msgstr "Кількість стовпчиків:"

#~ msgid "Open Command Bar"
#~ msgstr "Відкрити панель команд"

#~ msgid "About %1"
#~ msgstr "Про %1"

#~ msgid "About KDE"
#~ msgstr "Про KDE"

#~ msgid "No results found"
#~ msgstr "Нічого не знайдено"

#~ msgctxt "@action:menu"
#~ msgid "Quit Marknote"
#~ msgstr "Вийти з Marknote"

#~ msgid "Reset"
#~ msgstr "Скинути"

#~ msgid "New Note (%1)"
#~ msgstr "Нова нотатка (%1)"

#~ msgid "Add"
#~ msgstr "Додати"

#~ msgid "Add Notebook"
#~ msgstr "Додати записник"

#~ msgid "Notebook name"
#~ msgstr "Назва записника"

#~ msgctxt "@action:button"
#~ msgid "highlight"
#~ msgstr "підсвічування"

#~ msgctxt "@item:inmenu disc list style"
#~ msgid "Disc"
#~ msgstr "Диск"

#~ msgctxt "@item:inmenu circle list style"
#~ msgid "Circle"
#~ msgstr "Коло"

#~ msgctxt "@item:inmenu square list style"
#~ msgid "Square"
#~ msgstr "Квадрат"

#~ msgctxt "@item:inmenu numbered lists"
#~ msgid "123"
#~ msgstr "123"

#~ msgctxt "@item:inmenu lowercase abc lists"
#~ msgid "abc"
#~ msgstr "abc"

#~ msgctxt "@item:inmenu uppercase abc lists"
#~ msgid "ABC"
#~ msgstr "ABC"

#~ msgctxt "@item:inmenu lower case roman numerals"
#~ msgid "i ii iii"
#~ msgstr "i ii iii"

#~ msgctxt "@item:inmenu upper case roman numerals"
#~ msgid "I II III"
#~ msgstr "I II III"

#~ msgid "Heading %1"
#~ msgstr "Заголовок %1"
